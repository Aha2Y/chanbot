#!/usr/bin/python
from ConfigParser import RawConfigParser as ConfParser
from BeautifulSoup import BeautifulSoup
from optparse import OptionParser
from collections import deque
from time import sleep, time, gmtime, strftime
import ConfigParser
import os
import sys
import socket
import codecs
from irc import *
from select import select
import urllib
import imp
import modules.fml
import modules.meme
import modules.bitly

my = IRCbot()
start = time()

try:
   config = ConfigParser.RawConfigParser()
   config.readfp(codecs.open("config.ini", "r", "utf8"))
except IOError as config:
   sys.exit()
     
irc = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
irc.connect ((config.get('connection', 'server'),config.getint('connection', 'port')))
irc.send('NICK %s\r\n' % config.get('irc-bot', 'nickname'))
irc.send('USER %s %s internets :%s\r\n' % (config.get('irc-bot', 'nickname'), config.get('irc-bot', 'ident'), config.get('irc-bot', 'realname')))
while 1:
    (rl, wl, xl) = select([irc], [], [])
    for sock in rl:
        data = readline(sock)
        raw = parse(data)
        if sock == irc:
            print data
            if raw[0].lower() == "ping":
               irc.send('PRIVMSG %s :[IRCD] Received ping, Sending pong. \r\n' % config.get('irc-bot', 'logchan'))
               irc.send("PONG {reply}\r\n".format(reply = raw[1]))
            if raw[0].startswith("NickServ!"):
               if raw[1] == "NOTICE" and "This nickname is registered" in raw[3]:
                  irc.send('JOIN %s \r\n' % config.get('irc-bot', 'logchan'))
                  irc.send('PRIVMSG NickServ :identify %s \r\n' % config.get('irc-bot', 'nickpass'))
                  irc.send('PRIVMSG %s :[NickServ] Successfuly identifed with password. \r\n' % config.get('irc-bot', 'logchan'))
                  for channel in channels:
                     irc.send('JOIN %s \r\n' % channel)
                     irc.send('PRIVMSG %s :[Join] Joined %s \r\n' % (config.get('irc-bot', 'logchan'), channel))
            if raw[1] == "PRIVMSG":
               my.sender = raw[0]
               my.nick = raw[0].split("!")[0]
               my.auth = data.split('@')[0][1:]
               if len(raw) > 3:
                  if raw[3] == ".uptime":
                     end = time()
                     uptime = end - start
                     irc.send('PRIVMSG %s :[04Uptime] Services up for %s \r\n' % (raw[2], duration_human(uptime)))
                  if raw[3].startswith(".reload "):
                     try:
                        if config.get('staff', my.auth) == 'admin':
                           if raw[3].split(" ", 1)[1] == "fml":
                              imp.reload(modules.fml)
                              irc.send('PRIVMSG %s :Reloaded FML module.\r\n' % raw[2])
                           if raw[3].split(" ", 1)[1] == "meme":
                              imp.reload(modules.meme)
                              irc.send('PRIVMSG %s :Reloaded MEME module.\r\n' % raw[2])
                           if raw[3].split(" ", 1)[1] == "fml":
                              imp.reload(modules.bitly)
                              irc.send('PRIVMSG %s :Reloaded BITLY module.\r\n' % raw[2])
                     except ConfigParser.NoOptionError:
                        continue
                  if raw[3] == ".die":
                     try:
                        if config.get("staff", my.auth) == "admin":
                           irc.send('QUIT :Module unloaded\r\n' % raw[2])
                     except ConfigParser.NoOptionError:
                           irc.send('PRIVMSG %s :I don\'t reoganize you.\r\n' % raw[2])                        
                  if raw[3] == ".fml":
                     q = modules.fml.get()
                     irc.send('PRIVMSG %s :[04Fml/04%s] %s\r\n' % (raw[2], q.number, q.text))
                     sleep(01)
                     irc.send('PRIVMSG %s :%s - %s\r\n' % (raw[2], q.agree, q.disagree))
                  if raw[3] == ".meme":
                     q = modules.meme.get()
                     irc.send('PRIVMSG %s :[04Meme] %s\r\n' % (raw[2], q.meme))
                     imp.reload(modules.meme)
                  if raw[3].startswith(".slogan"):
                     try:
                        slogan_topic = raw[3].split(" ", 1)[1]
                        slogan_url = urllib.urlopen("http://parsers.faux-bot.com/slogan/%s" % slogan_topic) 
                        slogansoup = BeautifulSoup(slogan_url)
                        x = str(slogansoup) 
                        x = x.split()
                        slogan = " ".join(x[1:]).strip("End Response:")
                        irc.send('PRIVMSG %s :[04Slogan] "%s"\r\n' % (raw[2], slogan))
                     except:
                        irc.send('NOTICE %s :Syntax: .slogan <url>\r\n' % my.nick)     
                  if raw[3].startswith(".shorten"):
                     try:
                        shorten = raw[3].split(" ", 1)[1]
                        api = modules.bitly.Api(login='aha2y', apikey='R_68fd8f8405600aab706feecbc07ea066') 
                        short=api.shorten(shorten,{'history':1})
                        irc.send('PRIVMSG %s :[04Bitly] %s\r\n' % (raw[2], short))
                     except:
                        irc.send('NOTICE %s :Syntax: .shorten <url>\r\n' % my.nick)     