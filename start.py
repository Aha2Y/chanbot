from subprocess import Popen
from ConfigParser import RawConfigParser as ConfParser
from optparse import OptionParser
from collections import deque
import sys
import os,time
import subprocess
import signal
import imp
import ConfigParser
import datetime
import codecs
x = "bot.py"

try:
   config = ConfigParser.RawConfigParser()
   config.readfp(codecs.open("config.ini", "r", "utf8"))
except IOError as config:
   sys.exit()

pid = os.getpid() + 2
if __name__ == "__main__":
        if len(sys.argv) == 2:
                if 'start' == sys.argv[1]:
                        print("Starting daemon... - Pid: %s" % pid)
                        config.set('debug', 'debug', 'no')
                        if os.fork()==0:
                                os.setsid()
                                sys.stdout=open("/dev/null", 'w')
                                sys.stdin=open("/dev/null", 'r')
                                ret = subprocess.call([sys.executable, x])
                                sys.exit(2)
                elif 'stop' == sys.argv[1]:
                        os.system("killall -9 /usr/bin/python bot.py")
                elif 'debug' == sys.argv[1]:
                        config.set('debug', 'debug', 'yes')
                        print("Starting daemon... - Pid: %s" % pid)
                        config.set('debug', 'debug', 'no')
                        if os.fork()==0:
                                os.setsid()
                                sys.stdout=open("/dev/null", 'w')
                                sys.stdin=open("/dev/null", 'r')
                                ret = subprocess.call([sys.executable, x])
                                sys.exit(2)
                else:
                        print "Unknown command"
                        sys.exit(2)
                sys.exit(0)
        else:
                print "usage: %s start|stop|debug" % sys.argv[0]
                sys.exit(2)